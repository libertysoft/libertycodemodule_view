LibertyCodeModule_View
======================



Description
-----------

Library contains framework modules, 
to manage application rendering.

---



Requirement
-----------

- Script language: PHP: version 7 || 8

---



Installation
------------

Several ways are possible:

#### Composer

1. Requirement
    
    It requires composer installation.
    For more information: https://getcomposer.org
    
2. Command: Move in project root path
    
    ```sh
    cd "<project_root_path>"
    ```

3. Command: Installation
    
    ```sh
    php composer.phar require liberty_code_module/view ["<version>"]
    ```
    
4. Note

    - Include vendor
        
        If project uses composer, 
        vendor must be included:
        
        ```php
        require_once('<project_root_path>/vendor/autoload.php');
        ```
    
    - Configuration
    
        Installation command allows to add, 
        on composer file "<project_root_path>/composer.json",
        following configuration:
        
        ```json
        {
            "require": {
                "liberty_code_module/view": "<version>"
            }
        }
        ```

#### Include

1. Download
    
    - Download following repository.
    - Put it on repository root path.
    
2. Include source
    
    ```php
    require_once('<repository_root_path>/include/Include.php');
    ```

---



Application installation
------------------------

#### Configuration

1. Configuration: application module: "<project root path>/config/Module.<config_file_ext>"

    Add in list part, required modules:

    Example for YML configuration format, from composer installation:

    ```yml
    list: [
        {
            path: "/vendor/liberty_code_module/view/src/view",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/liberty_code_module/view/src/php_compiler",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        },
        {
            path: "/vendor/liberty_code_module/view/src/cache",
            config_parser: {
                type: "string_table_php",
                source_format_get_regexp: "#^\\<\\?php\\s*(.*)(\\s\\?\\>)?\\s*$#ms",
                source_format_set_pattern: "<?php \\n%1$s",
                cache_parser_require: true,
                cache_file_parser_require: true
            }
        }
    ]
    ```

---



Configuration
-------------

#### Application parameters configuration

- Use following file on your modules to configure specific elements
    
    ```sh
    <module_root_path>/config/ParamApp.php
    ```

- Elements configurables
    
    - Configuration to param template repository.
    
    - Configuration to param compiler.
    
    - Configuration to populate viewer additional arguments.
    
    - Configuration to param viewer.
    
    - Configuration to param cache.

---



Usage
-----

Following examples consider command line application relative path as 
"bin/app".

#### View

Features allow to set and manage rendering, available in application.

#### PHP compiler

Features allow to set and manage PHP compiler, available for application rendering.

#### Cache

Features allow to set and manage cache, available for application rendering.

_Example_

```sh
cd "<project_root_path>"
...
// Remove all items, from view cache
php bin/app view:cache:remove
```

---


