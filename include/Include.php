<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/view/library/ConstView.php');
include($strRootPath . '/src/view/helper/ViewerAddArgHelper.php');
include($strRootPath . '/src/view/boot/ToolBoxViewBootstrap.php');
include($strRootPath . '/src/view/boot/ViewBootstrap.php');

include($strRootPath . '/src/php_compiler/library/ConstPhpCompiler.php');
include($strRootPath . '/src/php_compiler/helper/PhpCompilerFormatHelper.php');

include($strRootPath . '/src/cache/library/ConstCache.php');
include($strRootPath . '/src/cache/controller/command/CacheController.php');
include($strRootPath . '/src/cache/boot/CacheBootstrap.php');