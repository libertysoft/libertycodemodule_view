<?php
/**
 * Description :
 * This class allows to define cache module bootstrap class.
 * Cache module bootstrap allows to boot cache module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\cache\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\library\path\library\ToolBoxPath;
use liberty_code_module\view\cache\library\ConstCache;



class CacheBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        AppInterface $objApp
    )
    {
        // Call parent constructor
        parent::__construct($objApp, ConstCache::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Create directories, if required
        $tabDirPath = array(
            ConstCache::CONF_PATH_DIR_VAR_FRAMEWORK_VIEW,
            ConstCache::CONF_PATH_DIR_VAR_FRAMEWORK_VIEW_CACHE
        );

        foreach($tabDirPath as $strDirPath)
        {
            // Get directory full path
            $strDirPath = ToolBoxPath::getStrPathFull($strDirPath);

            // Create directory, if required
            if(!is_dir($strDirPath))
            {
                mkdir($strDirPath);
            }
        }
    }



}