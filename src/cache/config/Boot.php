<?php

use liberty_code_module\view\cache\boot\CacheBootstrap;



return array(
    'view_bootstrap' => [
        'call' => [
            'class_path_pattern' => CacheBootstrap::class . ':boot'
        ]
    ]
);