<?php

use liberty_code\cache\format\model\FormatData;
use liberty_code\cache\repository\format\model\FormatRepository;
use liberty_code\file\register\directory\model\DirRegister;
use liberty_code_module\view\cache\controller\command\CacheController;



return array(
    // Cache services
    // ******************************************************************************

    'view_cache_file_register' => [
        'source' => DirRegister::class,
        'argument' => [
            ['type' => 'config', 'value' => 'view/cache/register/file/config']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'view_cache_register' => [
        'source' => DirRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'view_cache_file_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'view_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'view/cache/config'],
            ['type' => 'dependency', 'value' => 'view_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Cache controller services
    // ******************************************************************************

    'view_cache_command_controller' => [
        'source' => CacheController::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'view_cache_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Template repository cache services
    // ******************************************************************************

    'view_template_repository_cache_register' => [
        'source' => DirRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'view_cache_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'view_template_repository_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'view/template/repository/cache/config'],
            ['type' => 'dependency', 'value' => 'view_template_repository_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Compiler cache services
    // ******************************************************************************

    'view_compiler_cache_register' => [
        'source' => DirRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'view_cache_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'view_compiler_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'view/compiler/cache/config'],
            ['type' => 'dependency', 'value' => 'view_compiler_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Viewer cache services
    // ******************************************************************************

    'view_viewer_cache_register' => [
        'source' => DirRegister::class,
        'set' => ['type' => 'dependency', 'value' => 'view_cache_register'],
        'option' => [
            'shared' => true
        ]
    ],

    'view_viewer_cache_repository' => [
        'source' => FormatRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'view/viewer/cache/config'],
            ['type' => 'dependency', 'value' => 'view_viewer_cache_register'],
            ['type' => 'class', 'value' => FormatData::class],
            ['type' => 'class', 'value' => FormatData::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);