<?php

use liberty_code\cache\repository\format\model\FormatRepository;
use liberty_code\file\register\directory\model\DirRegister;
use liberty_code\framework\framework\library\path\library\ToolBoxPath;
use liberty_code_module\view\cache\library\ConstCache;



return array(
    // View configuration
    // ******************************************************************************

    'view' => [
        // Cache configuration
        'cache' => [
            // Register configuration
            'register' => [
                'file' => [
                    /**
                     * Configuration format:
                     * @see DirRegister configuration format.
                     */
                    'config' => [
                        'store_dir_path' => ToolBoxPath::getStrPathFull(ConstCache::CONF_PATH_DIR_VAR_FRAMEWORK_VIEW_CACHE),
                        'set_timezone_name' => 'UTC'
                    ]
                ]

            ],

            /**
             * Configuration format:
             * @see FormatRepository configuration format.
             */
            'config' => [
                'key_pattern' => 'view-%s',
                'key_regexp_select' => '#view\-(.+)#'
            ]
        ],

        'template' => [
            'repository' => [
                'config' => [
                    // Set specific value to prevent key name conflict with cache directory register
                    'key_path_separator' => '.'
                ],

                // Cache configuration
                'cache' => [
                    /**
                     * Configuration format:
                     * @see FormatRepository configuration format.
                     */
                    'config' => [
                        'key_pattern' => 'view-template-repository-%s',
                        'key_regexp_select' => '#view\-template\-repository\-(.+)#'
                    ]
                ]
            ]
        ],

        'compiler' => [
            // Cache configuration
            'cache' => [
                /**
                 * Configuration format:
                 * @see FormatRepository configuration format.
                 */
                'config' => [
                    'key_pattern' => 'view-compiler-%s',
                    'key_regexp_select' => '#view\-compiler\-(.+)#'
                ]
            ]
        ],

        'viewer' => [
            // Cache configuration
            'cache' => [
                /**
                 * Configuration format:
                 * @see FormatRepository configuration format.
                 */
                'config' => [
                    'key_pattern' => 'view-viewer-%s',
                    'key_regexp_select' => '#view\-viewer\-(.+)#'
                ]
            ]
        ]
    ]
);