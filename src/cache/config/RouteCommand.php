<?php

use liberty_code_module\view\cache\library\ConstCache;
use liberty_code_module\view\cache\controller\command\CacheController;



return array(
    // Cache routes
    // ******************************************************************************

    'view_cache_get_key' => [
        'source' => 'view:cache:get:key',
        'call' => [
            'class_path_pattern' => CacheController::class . ':actionGetKey'
        ],
        'description' => 'Get all keys, from view cache'
    ],

    'view_cache_remove' => [
        'source' => 'view:cache:remove',
        'call' => [
            'class_path_pattern' => CacheController::class . ':actionRemove'
        ],
        'description' => 'Remove specified or all items, from view cache',
        'source_argument' => [
            [
                'type' => 'option',
                'name' => [ConstCache::COMMAND_OPT_NAME_KEY, 'k'],
                'description' => 'Specified comma separated list of keys, to remove',
                'required' => false,
                'required_value' => true,
                'type_value' => 'string'
            ]
        ]
    ]
);