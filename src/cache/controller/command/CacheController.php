<?php
/**
 * Description :
 * This class allows to define cache controller class for command line.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\cache\controller\command;

use liberty_code\controller\controller\model\DefaultController;

use liberty_code\cache\repository\api\RepositoryInterface;
use liberty_code\request_flow\response\model\DefaultResponse;
use liberty_code\command\request_flow\front\model\CommandFrontController;
use liberty_code_module\view\cache\library\ConstCache;



class CacheController extends DefaultController
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Cache repository instance.
     * @var RepositoryInterface
     */
    protected $objCacheRepo;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param RepositoryInterface $objCacheRepo
     */
    public function __construct(
        RepositoryInterface $objCacheRepo
    )
    {
        // Init properties
        $this->objCacheRepo = $objCacheRepo;

        // Call parent constructor
        parent::__construct();
    }





    // Methods check
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    protected function checkAccessMethodEngine($strMethodNm, array $tabArg = null)
    {
        // Return result
        return true;
    }





    // Methods action
    // ******************************************************************************

    /**
     * Action to get keys,
     * from cache repository.
     *
     * @return DefaultResponse
     */
    public function actionGetKey()
    {
        // Init var
        $objCacheRepo = $this->objCacheRepo;

        // Get render
        $strRender = implode(
            PHP_EOL,
            $objCacheRepo->getTabSearchKey()
        );

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent($strRender . PHP_EOL);

        // Return response
        return $objResponse;
    }



    /**
     * Action to remove items,
     * from cache repository.
     *
     * @param CommandFrontController $objFrontController
     * @return DefaultResponse
     */
    public function actionRemove(
        CommandFrontController $objFrontController
    )
    {
        // Init var
        $tabKey = (
            (($key = $objFrontController->getOptValue(ConstCache::COMMAND_OPT_NAME_KEY, false)) !== false) ?
                array_map(
                    function($strKey) {return trim($strKey);},
                    explode(',', $key)
                ) :
                array()
        );
        $objCacheRepo = $this->objCacheRepo;

        // Remove specified items
        if(count($tabKey) > 0)
        {
            $objCacheRepo->removeTabItem($tabKey);
        }
        // Remove all items
        else
        {
            $objCacheRepo->removeTabItem($objCacheRepo->getTabSearchKey());
        }

        // Get response
        $objResponse = new DefaultResponse();
        $objResponse->setContent('[DONE]');

        // Return response
        return $objResponse;
    }



}