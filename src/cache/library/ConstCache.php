<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\cache\library;

use liberty_code\framework\framework\library\ConstFramework;



class ConstCache
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'view_cache';



    // Configuration command line arguments
    const COMMAND_OPT_NAME_KEY = 'key';



    // Path configuration
    const CONF_PATH_DIR_VAR_FRAMEWORK_VIEW = ConstFramework::CONF_PATH_DIR_VAR_FRAMEWORK . '/view';
    const CONF_PATH_DIR_VAR_FRAMEWORK_VIEW_CACHE = self::CONF_PATH_DIR_VAR_FRAMEWORK_VIEW . '/cache';



}