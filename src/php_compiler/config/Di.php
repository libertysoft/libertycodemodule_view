<?php

use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\compiler\format\model\FormatData;
use liberty_code\view\compiler\api\CompilerInterface;
use liberty_code\view\compiler\php\standard\model\StandardPhpCompiler;
use liberty_code_module\view\php_compiler\helper\PhpCompilerFormatHelper;



return array(
    // Compiler services
    // ******************************************************************************

    'view_php_compiler_format_data_before' => [
        'source' => FormatData::class,
        'option' => [
            'shared' => true,
        ]
    ],

    'view_php_compiler_format_data_after' => [
        'source' => FormatData::class,
        'option' => [
            'shared' => true,
        ]
    ],

    'view_php_compiler' => [
        'source' => StandardPhpCompiler::class,
        'argument' => [
            ['type' => 'class', 'value' => TmpRepositoryInterface::class],
            ['type' => 'config', 'value' => 'view/compiler/php/config'],
            ['type' => 'dependency', 'value' => 'view_compiler_cache_repository'],
            ['type' => 'dependency', 'value' => 'view_php_compiler_format_data_before'],
            ['type' => 'dependency', 'value' => 'view_php_compiler_format_data_after']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Compiler helper services
    // ******************************************************************************

    'view_php_compiler_format_helper' => [
        'source' => PhpCompilerFormatHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'view_php_compiler_format_data_before'],
            ['type' => 'dependency', 'value' => 'view_php_compiler_format_data_after']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    CompilerInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'view_php_compiler'],
        'option' => [
            'shared' => true,
        ]
    ]
);