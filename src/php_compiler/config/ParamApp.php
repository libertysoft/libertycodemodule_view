<?php

use liberty_code\view\compiler\php\standard\model\StandardPhpCompiler;



return array(
    // View configuration
    // ******************************************************************************

    'view' => [
        // Compiler configuration
        'compiler' => [
            'php' => [
                /**
                 * Compiler configuration format:
                 * @see StandardPhpCompiler configuration format.
                 *
                 */
                'config' => []
            ]
        ]
    ]
);