<?php
/**
 * Description :
 * This class allows to define PHP compiler format helper class.
 * PHP compiler format helper allows to provide features,
 * to manage formats, for PHP compiler.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\php_compiler\helper;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\view\compiler\format\model\FormatData;
use liberty_code\view\compiler\model\DefaultCompiler;



class PhpCompilerFormatHelper extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: PHP compiler format data instance, used to format render before compiling.
     * @var FormatData
     */
    protected $objPhpCompilerFormatDataBefore;



    /**
     * DI: PHP compiler format data instance, used to format render after compiling.
     * @var FormatData
     */
    protected $objPhpCompilerFormatDataAfter;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
     * @param FormatData $objPhpCompilerFormatDataBefore
     * @param FormatData $objPhpCompilerFormatDataAfter
     */
	public function __construct(
        FormatData $objPhpCompilerFormatDataBefore,
        FormatData $objPhpCompilerFormatDataAfter
    )
	{
        // Init properties
        $this->objPhpCompilerFormatDataBefore = $objPhpCompilerFormatDataBefore;
        $this->objPhpCompilerFormatDataAfter = $objPhpCompilerFormatDataAfter;

		// Call parent constructor
		parent::__construct();
	}





    // Methods setters
    // ******************************************************************************

    /**
     * Put specified format,
     * on PHP compiler formats,
     * used to format render before compiling.
     *
     * REGEXP pattern format:
     * @see DefaultCompiler::getStrCompileRenderFormat() format data key format.
     *
     * Format callable format:
     * @see DefaultCompiler::getStrCompileRenderFormat() format data value format.
     *
     * @param string $strRegexpPattern
     * @param callable $callableFormat
     * @return boolean
     */
    public function putFormatBefore(
        $strRegexpPattern,
        $callableFormat
    )
    {
        // Return result
        return $this
            ->objPhpCompilerFormatDataBefore
            ->putValue(
                $strRegexpPattern,
                $callableFormat
            );
    }



    /**
     * Put specified format,
     * on PHP compiler formats,
     * used to format render after compiling.
     *
     * REGEXP pattern format:
     * @see DefaultCompiler::getStrCompileRenderFormat() format data key format.
     *
     * Format callable format:
     * @see DefaultCompiler::getStrCompileRenderFormat() format data value format.
     *
     * @param string $strRegexpPattern
     * @param callable $callableFormat
     * @return boolean
     */
    public function putFormatAfter(
        $strRegexpPattern,
        $callableFormat
    )
    {
        // Return result
        return $this
            ->objPhpCompilerFormatDataAfter
            ->putValue(
                $strRegexpPattern,
                $callableFormat
            );
    }



    /**
     * Remove specified format,
     * from PHP compiler formats,
     * used to format render before compiling.
     *
     * @param string $strRegexpPattern
     * @return boolean
     */
    public function removeFormatBefore($strRegexpPattern)
    {
        // Return result
        return $this
            ->objPhpCompilerFormatDataBefore
            ->removeValue($strRegexpPattern);
    }



    /**
     * Remove specified format,
     * from PHP compiler formats,
     * used to format render after compiling.
     *
     * @param string $strRegexpPattern
     * @return boolean
     */
    public function removeFormatAfter($strRegexpPattern)
    {
        // Return result
        return $this
            ->objPhpCompilerFormatDataAfter
            ->removeValue($strRegexpPattern);
    }



}