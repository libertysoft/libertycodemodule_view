<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\view\boot;

use liberty_code\library\instance\model\Multiton;

use liberty_code\framework\module\api\ModuleInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\bootstrap\library\ToolBoxBootstrap;
use liberty_code\framework\framework\config\library\ToolBoxConfig;



class ToolBoxViewBootstrap extends Multiton
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();
	
	/**
	 * Only 1 instance authorized (Singleton)
     * @var int
     */
	static protected $__instanceIntCountLimit = 1;
	
	
	
	
	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Methods setters
	// ******************************************************************************

	/**
     * Put view configuration,
     * from specified configuration full file path.
     *
     * @param AppInterface $objApp
     * @param ModuleInterface $objModule
     * @param string $strFilePath
     * @return boolean
     */
    public static function putViewConfigFile(
        AppInterface $objApp,
        ModuleInterface $objModule,
        $strFilePath
    )
    {
        // Init var
        $result = ToolBoxBootstrap::putAppConfigFile(
            $objApp,
            $objModule,
            $strFilePath,
            'view'
        );

        // Return result
        return $result;
    }



    /**
     * Add specified template repository directory path.
     *
     * Directory path format:
     * @see DirTmpRepository configuration format, for one directory path.
     *
     * @param AppInterface $objApp
     * @param string|array $dirPath
     * @return boolean
     */
    public static function addTmpRepoDirPath(
        AppInterface $objApp,
        $dirPath
    )
    {
        // Init var
        $result = false;
        $objConfig = $objApp->getObjConfig();
        $strConfigKey = ToolBoxConfig::getStrPathKeyFromList(
            'view',
            'template',
            'repository',
            'config',
            'dir_path'
        );

        // Set directory path, on config, if required
        if(!is_null($objConfig))
        {
            // Get array of directory paths
            $tabDirPath = $objConfig->getValue($strConfigKey);
            $tabDirPath = (is_array($tabDirPath) ? $tabDirPath : array());

            // Register new directory path
            $tabDirPath[] = $dirPath;

            // Reset array of directory paths
            $result = ToolBoxBootstrap::putAppConfigConfigValue(
                $objApp,
                $strConfigKey,
                $tabDirPath
            );
        }

        // Return result
        return $result;
    }



    /**
     * Add specified viewer additional argument.
     *
     * @param AppInterface $objApp
     * @param string $strKey
     * @param mixed $value
     * @return boolean
     */
    public static function addViewerAddArg(
        AppInterface $objApp,
        $strKey,
        $value
    )
    {
        // Init var
        $result = false;
        $objConfig = $objApp->getObjConfig();
        $strConfigKey = ToolBoxConfig::getStrPathKeyFromList(
            'view',
            'viewer',
            'add_argument'
        );

        // Set argument, on config, if required
        if((!is_null($objConfig)) && is_string($strKey))
        {
            // Get array of arguments
            $tabArg = $objConfig->getValue($strConfigKey);
            $tabArg = (is_array($tabArg) ? $tabArg : array());

            // Register new argument
            $tabArg[$strKey] = $value;

            // Reset array of arguments
            $result = ToolBoxBootstrap::putAppConfigConfigValue(
                $objApp,
                $strConfigKey,
                $tabArg
            );
        }

        // Return result
        return $result;
    }



}