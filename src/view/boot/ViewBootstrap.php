<?php
/**
 * Description :
 * This class allows to define view module bootstrap class.
 * View module bootstrap allows to boot view module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\view\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\library\path\library\ToolBoxPath;
use liberty_code_module\view\view\library\ConstView;



class ViewBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     */
    public function __construct(
        AppInterface $objApp
    )
    {
        // Call parent constructor
        parent::__construct($objApp, ConstView::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Create directories, if required
        $tabDirPath = array(
            ConstView::CONF_PATH_DIR_RSC,
            ConstView::CONF_PATH_DIR_RSC_VIEW
        );

        foreach($tabDirPath as $strDirPath)
        {
            // Get directory full path
            $strDirPath = ToolBoxPath::getStrPathFull($strDirPath);

            // Create directory, if required
            if(!is_dir($strDirPath))
            {
                mkdir($strDirPath);
            }
        }
    }



}