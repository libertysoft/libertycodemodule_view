<?php

use liberty_code_module\view\view\boot\ViewBootstrap;



return array(
    'view_bootstrap' => [
        'call' => [
            'class_path_pattern' => ViewBootstrap::class . ':boot'
        ]
    ]
);