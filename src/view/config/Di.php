<?php

use liberty_code\data\data\table\model\TableData;
use liberty_code\di\provider\api\ProviderInterface;
use liberty_code\view\template\repository\api\TmpRepositoryInterface;
use liberty_code\view\compiler\api\CompilerInterface;
use liberty_code\view\viewer\api\ViewerInterface;
use liberty_code\view\viewer\standard\model\StandardViewer;
use liberty_code\view\view\factory\api\ViewFactoryInterface;
use liberty_code\view\view\factory\standard\model\StandardViewFactory;
use liberty_code\file\view\template\repository\directory\model\DirTmpRepository;
use liberty_code_module\view\view\helper\ViewerAddArgHelper;



return array(
    // Template repository services
    // ******************************************************************************

    'view_template_repository' => [
        'source' => DirTmpRepository::class,
        'argument' => [
            ['type' => 'config', 'value' => 'view/template/repository/config'],
            ['type' => 'dependency', 'value' => 'view_template_repository_cache_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Viewer services
    // ******************************************************************************

    'view_viewer_add_arg_data' => [
        'source' => TableData::class,
        'argument' => [
            ['type' => 'config', 'value' => 'view/viewer/add_argument']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'view_viewer' => [
        'source' => StandardViewer::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'view_template_repository'],
            ['type' => 'class', 'value' => CompilerInterface::class],
            ['type' => 'config', 'value' => 'view/viewer/config'],
            ['type' => 'dependency', 'value' => 'view_viewer_cache_repository'],
            ['type' => 'dependency', 'value' => 'view_viewer_add_arg_data']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Viewer helper services
    // ******************************************************************************

    'view_viewer_add_arg_helper' => [
        'source' => ViewerAddArgHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'view_viewer_add_arg_data']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // View services
    // ******************************************************************************

    'view_factory' => [
        'source' => StandardViewFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'view_viewer'],
            ['type' => 'mixed', 'value' => null],
            ['type' => 'class', 'value' => ProviderInterface::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    TmpRepositoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'view_template_repository'],
        'option' => [
            'shared' => true
        ]
    ],

    ViewerInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'view_viewer'],
        'option' => [
            'shared' => true
        ]
    ],

    ViewFactoryInterface::class => [
        'set' => ['type' => 'dependency', 'value' => 'view_factory'],
        'option' => [
            'shared' => true
        ]
    ]
);