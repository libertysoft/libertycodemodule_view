<?php

use liberty_code\view\viewer\standard\model\StandardViewer;
use liberty_code\file\view\template\repository\directory\model\DirTmpRepository;
use liberty_code\framework\framework\library\path\library\ToolBoxPath;
use liberty_code_module\view\view\library\ConstView;



return array(
    // View configuration
    // ******************************************************************************

    'view' => [
        // Template configuration
        'template' => [
            'repository' => [
                /**
                 * Template repository configuration format:
                 * @see DirTmpRepository configuration format.
                 */
                'config' => [
                    'dir_path' => [
                        ToolBoxPath::getStrPathFull(ConstView::CONF_PATH_DIR_RSC_VIEW)
                    ],
                ]
            ]
        ],

        // Viewer configuration
        'viewer' => [
            /**
             * Viewer configuration format:
             * @see StandardViewer configuration format.
             */
            'config' =>[],

            // Array of additional arguments
            'add_argument' => []
        ]
    ]
);