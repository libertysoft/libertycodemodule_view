<?php
/**
 * Description :
 * This class allows to define viewer additional argument helper class.
 * Viewer additional argument helper allows to provide features,
 * to manage additional arguments, for viewer.
 * 
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\view\helper;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\data\data\table\model\TableData;



class ViewerAddArgHelper extends DefaultBean
{
	// ******************************************************************************
	// Properties
	// ******************************************************************************
	
	/**
	 * Init instances table to dissociate this class from parent
     * @var array
     */
	static protected $__instanceTab = array();



    /**
     * DI: Viewer additional argument data instance.
     * @var TableData
     */
    protected $objViewerAddArgData;




	
	// ******************************************************************************
	// Methods
	// ******************************************************************************
	
	// Constructor / Destructor / Others
	// ******************************************************************************

	/**
	 * @inheritdoc
     * @param TableData $objViewerAddArgData
     */
	public function __construct(
        TableData $objViewerAddArgData
    )
	{
        // Init properties
        $this->objViewerAddArgData = $objViewerAddArgData;

		// Call parent constructor
		parent::__construct();
	}





    // Methods setters
    // ******************************************************************************

    /**
     * Put specified argument,
     * on viewer additional arguments.
     *
     * @param string $strKey
     * @param mixed $value
     * @return boolean
     */
    public function putArg(
        $strKey,
        $value
    )
    {
        // Return result
        return $this
            ->objViewerAddArgData
            ->putValue(
                $strKey,
                $value
            );
    }



    /**
     * Remove specified argument,
     * from viewer additional arguments.
     *
     * @param string $strKey
     * @return boolean
     */
    public function removeArg($strKey)
    {
        // Return result
        return (
            $this->objViewerAddArgData->checkValueExists($strKey) ?
                $this->objViewerAddArgData->removeValue($strKey) :
                false
        );
    }



}