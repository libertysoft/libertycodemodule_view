<?php
/**
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace liberty_code_module\view\view\library;



class ConstView
{
	// ******************************************************************************
	// Constants
	// ******************************************************************************

    // Configuration module
    const MODULE_KEY = 'view_view';



    // Path configuration
    const CONF_PATH_DIR_RSC = '/resource';
    const CONF_PATH_DIR_RSC_VIEW = self::CONF_PATH_DIR_RSC . '/view';



}